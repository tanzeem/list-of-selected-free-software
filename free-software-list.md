## Selected FOSS Applications (updated October 2018)


The following are almost ready to deploy projects in all possible sections that are scientifically reasonable to try for a community. This list is not about technology. This list reflects several labour hours of several individuals and communities who dedicate to create a technology or tool that are progressively & scientifically useful, democratic, privacy friendly, decentralized, federated aware. While some of the tools/apps could be installed by the individual itself, some may require community effort.

## Open Hardware Projects

### Peer Production

-    P2P Foundation - https://p2pfoundation.net

-    Sensorica open value 

-    journal of peer production  https://peerproduction.net

-    Apertus - open source cinema - https://apertus.org

-    FOSMC - open source motor cycle https://fictiv.com/blog/fosmc

-    Fusor 

-    Novena - Laptop

-    Olimex - Teres - Laptop

-    OpenMoko

-    Fusor

-    Kano

-    OpenMoko

-    Wevolver.com


-    Knitic

-    Laser Saur

-    MicroSlicer

-    Novena - Laptop

-    Olimex - Teres - Laptop

-    OpenMoko

-    RepRap

-    FabFM

-    Precious 

-   WASP https://www.wasproject.it

-    Dropfab (openspaceagency)

-    Stemcase (openspaceagency)


### Citizen Science

-    Foldscope

-    Mobile ECG

-    Gama Cardio ECG

-    Open BCI

-    Open curiosity Rover - NASA

-    PocketScience Labs

-    RamanPI

-    SATNOGS

-    SmartCitizen Kit

-    Tricorder

-    UltraScope

-    uRadMonitor

-    Spectruino

-    Altusmetrum : http://altusmetrum.org - High powered model rocketry


### Community Labs

-    Appropedia

-    Sensorica

-    Public labs

-    AREN by NASA - https://www.globe.gov/web/aren-project

-    Open space agency

-    Hackaday

-    Fullmotion Dynamics


### Community Networks

-    HackRF

-    Minima Radio Transciever

-    Openduos - HAM - Upconvertor

-    PSDR

-    SoftRock SDR

-    Turris

-    Myriad RF

-    http://smcelectronics.com/download.htm


### Self hosting

-    FreedomBox (https://wiki.debian.org/FreedomBox/Features)

-    FreedomBone (https://freedombone.net/apps.html)

-    Yunohost (https://yunohost.org/#/apps)


### Network of Things (a.k.a. IOT)

* Hardware - Platform

-    Arduino

-    ESP8266

-    Beaglebone

-    Raspberry pi

-    Orange  pi

-    Open hardware.io

-    Mysensors.org


* OS - Platform

-    RIOT OS

-    Platform IO 

-    Openhab server


### Simulation / Evaluation

-    Cup Carbon



### Mobile - Android

* Market stores

    - Fdroid


* Applications

   - Blockada

   - Carnet

   - Ncalc+

   - Stellarium

   - OSMAnd

   - Windy

   - StreetComplete

   - OpenMapkit

   - Vespucci

   - CitiZen

   - Transportr

   - Critical Maps

   - MapTrek

   - Traccar Coient, Manager

   - Exodus Privacy

   - Feeder

   - GPS Test

   - SatStat

   - GNUCash

   - Indic Keyboard

   - LibreTorrent

   - ServalMesh

   - Plumble

   - Orbot

   - Trebleshot

   - Silence

   - Tusky

   - Dandelion

   - Riot

   - Wire

   - Ring

   - JitsiMeet

   - Wiregaurd

   - Zulip

   - Commons

   - WikiJourney

   - Wikipedia

   - Kiwix

   - Keypass DX

   - POpenkeychain

   - NewPipe

   - VLC

   - Popcorntime

   - PixelKnot

   - CameraRoll

   - OpenCam

   - Retroboy

   - Call Recorder

   - Audio Recorder

   - Book Reader

   - Barcode Scanner

   - Dairy

   - Editor

   - Tuta Nota

   - Habits

   - Screen Stream

   - SecondScreen

   - Kaleidoscope

   - Labcoat


* Development

   - Android Studio

   - Flutter



### Decentralization, Collaboration, Contribution

* Contributing Computation

   - BOINC


* Sharing

   - Retroshare

   - Nitroshare

   - Syncthing

   - Filezilla

   - Transmission

   - Torrential


### Social Messaging & Networking

   - RIOT/Matrix

   - Mastodon

   - Diaspora

   - Ssb



### Geography - Maps - Cartography

   - OSgeo

   - JOSM

   - iD

   - POSM

   - Tasking Managerretfd

   - OSMAnd, Street Complete

   - QGIS, GrassGIS, SACP, gv-SIG, OTB, uDig, SAGA, SNAP

   - OpenRefine, OSMembrane

   - Three js

   - Turf js

   - Tippecanoe

   - Tile light server

   - Tilemill, tileoven

   - Vector time mapper 

   - Deck

   - Qground control


### Designer's Table

* Sketching, Illustration, Wireframe, Flow, Rapid prototyping

  -  Inkscape

  -  Gimp

  -  Pencil

  -  Screen ruler


* Font Design, Typography, Calligraphy

  -  Fontforge

  - Birdfont

  -  Fork Awesome


* Mobile UX

  -  Android Asset Studio

  -  Android Icon Animator

  -  Color- Brewer

  -  Layer - Visualizer

  -  Shape Shifter

  -  Material Design Icons


* AudioPhiles (audio production)

  -  Puredata

  -  LMMS

  -  Audacity

  -  Ardour

  -  Sonic visualizer

  -  Baudline

  -  Faust

  -  Owl - hoxtownowl

  -  Musichackspace

  -  Ambitools

  -  Hoalibrary - high order ambisonics library

  -  Bela - platform for audio interaction

  -  Ear tone toolbox


### Desktop Application Design & Development

  -  C++/Vala/Python/JS + GNOME/GTK stack

  -  Java + Scenebuilder

  -  Pothos Flow + SDR instruments

  -  Glade for UI Layout & Design

  -  Eclipse tools 


* IDE

  -  Vim or emacs or spacemacs with plugins

  -  Eclipse 

  -  Builder

  -  Atom

  -  Vstudio code

  -  Codeblocks

  -  Jupyter notebook


* Generative Art:

  -  Alchemy

  -  Processing

  -  Open frameworks

  -  Puredata


* Dataflow, Functional Reactive, Asynchronous Programming

  -  Sodium

  -  Luna

  -  Flapjax in js

  -  Xod

  -  Nodered 

  -  Noflo js

  -  Zaluum

  -  Pothosware 

  -  GNURadio Companion 


* Embedded Systems Design & Development

  -  Embedded linux

  -  Platform IO + Atom IDE

  -  Openwrt for wireless 


### Radio Stuff

  -  GNURadio

  -  LuaRadio

  -  Pothos


### Web Design & Development

* Front End

  -  HTML + CSS : https://internetingishard.com

  -  CSS : https://cssreference.io/

  -  D3 js

  -  snap js

  -  mo js

  -  apexcharts 


* Backend

  -  jquery

  -  Vue js

  -  Cycle js

  -  sigma js

  -  turf js


### Computer Aided Design & Drafting

  -  Free CAD

  -  OpenSCAD

  -  KiCAD


### Open access Peer reviewed Scientific Publishing Network

* Document, Journal

  -  Latex /xetex

  -  Osp.kitchen/tools

  -  DAT Protocol

  -  Science fair

  -  Scientila

  -  Zotero

  -  Wallabag

  -  Academic torrents

  -  Sci-hub


* Stories

  -  Storyboarded

  -  Script visualizer


* Journalism

  -  Tor, Tails

  -  Timeline js


### Creative Content

* Video editor

- Kdenlive
- Openshot
- Shotcut
- Natron

* Screen recorder

- Kazam
- Obs studio
- Simple screen recorder

* Interactive  art

  -  ccv.nuigroup.com

  -  tuio.


### MAGAZINE'S for Critical 

  -  https://logicmag.io/

  -  Roar magazine

  -  Tni longreads

  -  A list apart
